
#Module for identifying at risk patients based on potassium status and aldo values

import pandas as pd
import numpy as np

def atRisk(data, buffer, threshold =15, aldoParameter = 'ALDO_VALUE'):
    """
    Module for identifying at risk patients based on potassium status\
    and aldo values.
    
    Takes DataFrame with PA screens and returns DataFrame of patients that are\
    at risk for having a hypokalemia induced false negative PA screen by\
    selecting for patients who are hypokalemic and have aldosterone values\
    fall within a pre-specified borderline zone. This borderline parameter is\
    accomplished by setting the upper bound as the aldosterone threshold value\
    for a positive PA screen (for conservative criteria this is 15) and lower\
    bound is set by passing a ‘buffer’ value which subtracts the presumed\
    change in aldosterone due to hypokalemia from the aldosterone threshold\
    value.
    
    Parameters:
    data (pandas DataFrame): DataFrame of patients and potassium values, indexed according to PA encounters 
    buffer : delta aldosterone under investigation
    threshold : minimum aldosterone concentration for positive screen
    aldoParameter : column name for patient aldosterone values
    
    Returns:
    atRiskTests (pandas DataFrame): original DataFrame filtered of patients in 'borderline' zone
    
    """
    
    atRiskTests = data[(data['K_STATUS'] == 1) &\
        (data[aldoParameter] > (threshold - buffer)) &\
        (data[aldoParameter] < threshold)]
   
    return atRiskTests

def chooseK(data, dataK):
    import pandas as pd
    #Merge Aldo-Renin Data With Potassium Results 
    tmp = data.reset_index()
    mergedKData = pd.merge(tmp, dataK.loc[:, ['UNI_ID', 'K_ENC_DATE', 'K_RESULT_VALUE']], on='UNI_ID', how='left')
    #Select potassium results Nearest in time to Aldo-Renin Encounter
    #Calculate Date difference in time between K test and aldo/renin test
    mergedKData['DATE_DIFF'] = (mergedKData.loc[:, 'RAR_ENC_DATE']- mergedKData.loc[: , 'K_ENC_DATE']).abs()
    #Choose nearest potassium test to each RAR test
    tmp = mergedKData.set_index(['UNI_ID', 'RAR_ENC_DATE']).sort_values('DATE_DIFF').sort_index()
    protoNearestK = tmp.groupby(level=[0,1]).first()
    #Filter for K tests only within 30 days of RAR test
    thirtyDays = pd.to_timedelta('30 days')
    thirtyDaysSeconds = thirtyDays / pd.offsets.Second(1)
    protoNearestK.K_RESULT_VALUE = protoNearestK.K_RESULT_VALUE.where(protoNearestK.DATE_DIFF.between(0 , thirtyDaysSeconds))
    nearestK = protoNearestK.copy()
    return nearestK

def repeatDiffCalc(data, diffy):
    """
    Calculate the difference across selected repeat tests.
    
    Takes pandas DataFrame of individual PA screens and their associated lab tests and calculates difference across repeated lab
    tests for each patient on specified column. Returns Pandas Series of the difference calculated indexed on original paired
    PA screen (UNI\_ID and ORDER\_START\_DATE).
    
    Parameters:
    data (pandas DataFrame): DataFrame of patients and lab values of interest, indexed according to PA encounter (ID, Date) 
    diffy (str): Column name in data to calculate the difference across repeat
    
    Returns: 
    delta (pandas Series): Series of RAR encounters and their
    
    """
    
    tmp = data.copy()
    
    #Sort index to put the results in date order, to facilitate change calculation
    tmp.sort_index(level=1, inplace=True)
    
    #Calculate the difference between subsequent tests for an individual patients
    delta = tmp.groupby(level=0)[diffy].diff()
    
    return delta

def meetsCriteria(data, aldoParm = 'ALDO_VALUE', reninParm = 'RENIN_VALUE', arrParm = 'ARR_RESULT_VALUE',\
                  arr=30, aldo=15, renin=0.5):
    """
    Pass dataframe and selected criteria and return dataframe containing only entries that meet criteria
    
    Takes DataFrame of individual PA screens and associated data and returns DataFrame of PA screens that meets pre-specified
    criteria on aldosterone, renin, and ARR values by filtering originial DataFrame. Default values are set to conservative
    criteria (ARR > 30, aldo > 15, renin < 0.5).
    
    Parameters:
    data (pandas DataFrame): DataFrame of patients and lab values of interest, indexed according to PA encounter
    aldoParm (str): Aldosterone value column name, defaults to 'ALDO_VALUE'
    reninParm (str): Renin value column name, defaults to 'RENIN_VALUE'
    arrParm (str): ARR value column name, defaults to 'ARR_RESULT_VALUE'
    arr (float): Threshold ARR value to filter on, defaults to 30
    aldo (float): Threshold aldo value to filter on, defaults to 15
    renin (float): Threshold renin value to filter on, defaults to 0.5
    
    Returns:
    criteriaMet (pandas DataFrame): Originial DataFrame with only patients meeting criteria

    """
    
    tmp = data.copy()
    criteriaMet = \
    tmp[\
        (tmp[arrParm] > arr) &\
        (tmp[aldoParm] > aldo) &\
        (tmp[reninParm] < renin)\
       ]
    return criteriaMet

def dataCleanAndCalc(dataInput):
    """
    Function used to pair Aldosterone and Renin results taken at the same encounter and used to calculte ARR
    
    Takes pandas DataFrame composed of each aldosterone and renin result (with patient and encounter information) and returns
    DataFrame of paired aldosterone and renin results (based on patient (UNI\_ID') and the time associated with each PA screen
    (ORDER\_START\_DATE)) as a single data entry. Non-paired tests and non-aldosterone or renin tests are deleted. Also
    calculates and creates column for the ARR for each paired test.
    
    Parameters: 
    dataInput (pandas DataFrame): DataFrame of all patients with Aldosterone and Renin results
    
    Returns:
    correctedInlineData (pandas DataFrame): DataFrame with paired Aldosterone & Renin results with encounter data
    
    """

    
    #Replace 'lower and upper limit' string test result values with their numeric equivalent
    data = dataInput.copy()
    data.loc[:, 'RESULT_VALUE'] = data.loc[:, 'RESULT_VALUE'].\
    replace(['<', '>'], '',regex=True)
    data.loc[:, 'RESULT_VALUE'] = pd.to_numeric(data.loc[:, 'RESULT_VALUE'],\
                                                       errors = 'coerce')
    
    #Convert float type time associated with test result to datetime type date
    tmpx = data.copy()
    tmpy = tmpx.reset_index()
    tmpy['ORDER_START_DATE'] = pd.to_datetime(tmpy.ORDER_START_DATE)
    data['HOUR'] = tmpy.ORDER_START_DATE.dt.hour
    
    #Remove any duplicated result entries
    data.drop_duplicates(inplace=True)

    
    #Create new columns for aldosterone and renin results. Each row will have the aldosterone\
    #and renin result that corresponds to that specific encounter
    
    #Will populate results initially using np.where function to copy a 'RESULT_VALUE' from an aldosteorne entry\
    #to 'ALDO_VALUE', same for a renin entry
   

    data['ALDO_VALUE'] = np.where(data['RESULT_ITEM_CODE'].str.contains('ALDOSTERONE'),\
                                  data['RESULT_VALUE'], np.nan)
    data['RENIN_VALUE'] = np.where(data['RESULT_ITEM_CODE'].str.contains('RENIN'),\
                                  data['RESULT_VALUE'], np.nan)
    
    #Merge corresponding aldo and renin values using a groupby on Patient ID and Order Time associated with each test\
    #and using the groupby mean function to essentially copy the corresponding value to complementary test row
    #Mean function between a value and a NaN will populate complementary test column with the value
    inlineValues = data.groupby(['UNI_ID', 'ORDER_START_DATE'])[['ALDO_VALUE', 'RENIN_VALUE']].agg('mean')
    data.set_index(['UNI_ID', 'ORDER_START_DATE'], inplace=True)
    
    #pd.Update to merge Aldo renin values with initial larger data frame
    data.update(inlineValues)
    
    #Calculate ARR for each encounter
    data['ARR_RESULT_VALUE'] = data['ALDO_VALUE']/data['RENIN_VALUE']
    
    #Remove possible unwanted or incorrect aldo and renin ARR_results
    #ORDER_ITEM_CODE(s) correspond to order item codes for all aldo renin tests in system

    correctedInlineData = data[data['ORDER_ITEM_CODE'].isin(['C9010005', 'C9010071',\
                                                                                 'Q16845','Q16846', 'ALREACT'])]


    
    return correctedInlineData

def originalValue(data, valueColumn, deltaColumn):
    '''
    Function to determine initial value of repeated tests
    
    Takes DataFrame with columns for current and delta values for a particular repeated lab value across a single patient and
    returns pandas series with original value of repeated test. This function is to be used with function repeatDiffCalc.
    
    Parameters:
    data (pandas DataFrame): DataFrame of lab values and calculated indexed on PA encounter
    valueColumn (str): Column name for current lab value 
    deltaColumn (str): Column name for previously calculated difference in lab values across repeated tests
    
    
    Returns:
    '''
    originalValue = data.loc[: , valueColumn] - data.loc[:, deltaColumn]
    return originalValue



def gfrExclusion(data, gfrList):
    '''
    Function to remove patients on exclusion criteia list
    '''
    tmp = data.copy()
    tmpR = tmp.reset_index()
    excluded = tmpR[~(tmpR['UNI_ID'].isin(gfrList))]
    
    return excluded


def prepControls(file):
    list_controlSelects = []
    for chunk in pd.read_csv(file, usecols=['UNI_ID_x', 'UNI_ID_y', 'AGE','AGE_MATCH','GENDER_MASTER_CODE','K_ENC_DATE'], chunksize=100000):
        chunk['FILTER_KEY'] =  chunk["UNI_ID_x"].map(str) + chunk["AGE_MATCH"]
        list_controlSelects.append(chunk)
        

    return list_ControlSelects



def filterKeyList(file):
    for chunk in pd.read_csv(file, usecols=['UNI_ID_x', 'UNI_ID_y', 'AGE','AGE_MATCH','GENDER_MASTER_CODE','K_ENC_DATE'],\
                              chunksize=100000):
        chunk.dropna(subset=['AGE', 'GENDER_MASTER_CODE'], how='any', inplace=True)
        #Select demographic information for first ecnounter
        #Select for each unique demo age match group
        chunk.drop_duplicates(subset=['UNI_ID_x', 'AGE_MATCH'], inplace=True)
        #Select for age match group at initial encounter
        chunkX = chunk.sort_values('AGE_MATCH').groupby('UNI_ID_x').first().reset_index()
        tmp_list.append(chunkX)
        
    return tmp_list
  
