# Hypokalemia Induced False Negative PA Screening

## Background
We sought to investigate the relationship between potassium status and PA screening within our health system and determine if hypokalemia was a clinically significant confounder factor. To that end to we sought to characterize the current PA screening practice, including rates of screening, potassium status, and screens performed while hypokalemic. Additionally, we sought to understand the relationship between potassium status and aldosterone concentration by investiagting changes in aldosterone and potassium across repeat tests.

The data analysis for our work on false negative PA screens is organized into a series of Jupyter Notebooks, organized by major areas of data wrangling and data analysis.

## Data
Data was extracted from Penn Medicine's Penn Data Store. The Jupyter notebooks can only function with the original data from Penn Data Store. At this time, we are unable to provide the original patient information, so the notebooks are unable to be deployed locally. 

The DataFrames in the notebooks are organized around individual PA screens, indexed by both patient ID and PA screen date. Other lab tests of interest, such as a potassium test, are organized according to patient ID, and the test within 30 days and closest in time to each PA screen for each patient is selected.  This selected lab test is then indexed in the dataframe according to its associated PA screen. When calculating the difference in repeat lab tests, the delta values is indexed according to the subsequent (repeat) lab test's associated PA screen. Hence, generally each datapoint is associated with one PA screen, and each PA screen is associated with one or no delta values. 

## Notebooks
| Notebook | Description |
|-----------------------------|------------------------------------------------------------------------------|
| Patient\_Population.ipynb | Selection of patient populations from clinical database |
| ControlGroupSelection.ipynb | Control group selection |
| Demographic\_Data.ipynb | Analysis of demographic information for PA screened and control patients |
| Screening.ipynb | Analysis of all patients and initially negative patients with repeat PA screening |
|Logistic\_Regression.ipynb | Analyses of factors associated with PA positivity on repeat | 
| Changes\_In\_Potassium.ipynb | Analyze the relationship between aldosterone and changes in potassium and potassium status on repeat screens |
| At\_Risk.ipynb | Identify patients with potentially hypokalemia masked positive PA screens |

### Patient\_Population
- Generation of DataFrames for paired Aldo-Renin tests with associated encounter information, potassium, sodium, bicarbonate, control group, and demographic information

### ControlGroupSelection
- DataFrame is filtered for age decile at inital PA encounter
- 10:1 controls are for each gender and age decile

### Demographic\_Data
- Tabulated age and demographic data summary stats for control and PA screened patients
- Tabulated summary stats for potassium, sodium, and bicarbonate labs for control and PA screened patients

### Screening
- Stats for hypokalemia rates among PA patients and control patients, with tests for significance
- Stats for patients meeting conservative criteria across several patient populations (all, hypokalemic, non-hypokalemic), with tests for significance. 
- Repeat and repeat sucsess rate for patients not initially meeting conservative criteria and hypokalemic patients.
- Aldosterone values for patients with repeat tests, broken down by conservative criteria on initial testing and repeat.
- Test for independence between meeting conservative criteria on repeat  and initial potassium status
- Test for independence between meeting conservative criteria on repeat and correction of hypokalemia vs all other potassium trajectory groups 
- Test for independence between meeting conservative criteria and correction of hypokalemia vs uncorrected hypokalemia
- Potassium supplementation rate, time deltas, and associated PA screens 

### Logistic Regression
- Construction of dataset and logistic regression analyzing association between selected factors and PA positivity on repeat PA screen 

### Changes\_In\_Potassium
- Orthogonal linear regression for delta potassium and delta aldosterone for all pairs of repeated tests
- Assignment of potassium trajectory groups in patients with repeated initial PA and potassium screens
- Delta aldosterone, delta potassium, and delta time calculated for each of the four potassium trajectory groups
- Violin plots for change in aldosterone for all potassium trajectory groups.

### At\_Risk
- Analyze borderline crtieria in patients with repeated PA screens and corrected hypokalemia
- Estimate number of patients with unmeasured hypokalemia and positive repeats 
- Estimate number of uncorrected hypokalemic, initially negative patients who would be positive on repeat
- Identify patients high risk, 'borderline' PA screens (initially negative, elevated aldosterone, hypokalemia) 

## Environment
- Docker
    - docker pull jupyter/scipy-notebook:17aba6048f44
- Python & packages
    - Python v2.7.5
    - Numpy v1.12.1
    - Pandas v0.20.1
    - Matplotlib v2.0.2
    - Seaborn v0.7.1
    - SciPy v0.19.0
    - StatsModels v0.8.0
    - Sklearn v0.20.2

## Authors
Nicholas W Rizer, Xiuro Ding, Debbie L. Cohen, Daniel S Herman
